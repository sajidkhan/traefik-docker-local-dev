# Using Traefik, Docker, and Let's Encrypt to manage multiple local development projects

## Simple Usage

1. clone the repository
2. create an environment file from the `.env.example` file called `.env` 

```shell
cp .env.example .env
```

3. edit the `.env` file and add your digital ocean token and the domain name.